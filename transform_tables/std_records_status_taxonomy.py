import itertools
import pickle
from datetime import datetime

import psycopg2 as pg
from psycopg2._psycopg import AsIs
from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, concat, lit, udf, when
from pyspark.sql.types import FloatType, StringType, TimestampType
from rdflib import XSD, Literal, Namespace, URIRef
from spark_etl_utils import Transform
from spark_etl_utils.coordinates.species_conservation_status import (
    generalise_latitude_for_udf,
    generalise_longitude_for_udf,
)
from spark_etl_utils.database import (
    get_db_query_pg,
    get_db_table_pandas,
    get_postgres_url,
    save_dataframe_as_jdbc_table,
)
from spark_etl_utils.rdf import (
    RECORD_TYPE_OBSERVATION,
    UNIQUE_ID,
    RDFDataset,
    generate_rdf_graph,
    get_mappings,
)
from spark_etl_utils.rdf.models import (
    FeatureOfInterest,
    Instant,
    Observation,
    Taxon,
    generate_underscore_uri,
)

from config import Config
from transform_tables.common import (
    create_point_and_regions,
    fix_cassowary,
    fix_date,
    post_transform,
)


class Table(Transform):
    def __init__(self, spark: SparkSession):
        super().__init__()
        self.spark = spark
        self.table = "std_misc_records_status_taxonomy"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE
        self.matched_species = {}

        try:
            with open("regions_cache.pkl", "rb") as cache:
                # with open(""C:\\Users\\uqjsanc2\\PycharmProjects\\etl-spark-williams\\regions_cache.pkl"", "rb") as cache:
                self.regions_cache = pickle.load(cache)
                print(len(self.regions_cache))
        except Exception as e:
            print("Error unpickling")
            self.regions_cache = {}

        query = """
                    ( select
                        std.will_species,
                        std.latdecimal,
                        std.longdecimal,
                        std.originalfullname,
                        std.scientificname,
                        std.scientificname as scientificname_attr,
                        std.authorship,
                        std."rank",
                        std.kingdom,
                        std.phylum,
                        std."class",
                        std."order",
                        std."family",
                        std.genus,
                        std.alaid,
                        std.common_name,
                        std.nca_status,
                        std.epbc_status,
                        std.significant,
                        std.confidential,
                        std.endemicity,
                        std.authority,
                        std.generalisation,
                        std.geom,
                        std.date_time as default_datetime,
                        concat('std-',std.id) as animal_occurr_id,
                        concat('std-',std.id) as unique_id
                    from
                        williams_wet_tropics_vertebrate_dataset.std_records_status_taxonomy std
                    where
                        std.area = 'WET'
                        --and std.will_species in ('Gavicalis chrysops','Phylidonyris nigra', 'Heteromyias albispecularis', 'Carternornis leucotis', 'Lophorina victoriae', 'Morelia amethistina', 'Saproscincus spp.', 'Lygisaurus spp.', 'Chaerophon jobensis', 'Certhionyx niger')
                    union
                    select
                        misc.will_species,
                        misc.latdecimal,
                        misc.longdecimal,
                        misc.originalfullname,
                        misc.scientificname,
                        misc.scientificname as scientificname_attr,
                        misc.authorship,
                        misc."rank",
                        misc.kingdom,
                        misc.phylum,
                        misc."class",
                        misc."order",
                        misc."family",
                        misc.genus,
                        misc.alaid,
                        misc.common_name,
                        misc.nca_status,
                        misc.epbc_status,
                        misc.significant,
                        misc.confidential,
                        misc.endemicity,
                        misc.authority,
                        misc.generalisation,
                        misc.geom,
                        misc.date_time as default_datetime,
                        concat('misc-', misc.id) as animal_occurr_id,
                        concat('misc-', misc.id) as unique_id
                    from
                        williams_wet_tropics_vertebrate_dataset.misc_records_status_taxonomy misc
                    where
                        misc.area = 'WET'
                        --and misc.will_species in ('Gavicalis chrysops', 'Phylidonyris nigra', 'Heteromyias albispecularis', 'Carternornis leucotis', 'Lophorina victoriae', 'Morelia amethistina', 'Saproscincus spp.', 'Lygisaurus spp.', 'Chaerophon jobensis', 'Certhionyx niger')
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["id"].isin(list(range(1, 100))))

    def load_lookup(self):
        dataset_version = get_db_table_pandas(
            self.db_host,
            self.db_port,
            self.db_name,
            "r_ecoplatform_metadata",
            self.db_username,
            self.db_password,
            Config.DATASET,
            query="select load_date from {}.r_ecoplatform_metadata".format(Config.DATASET),
        ).iloc[0, 0]

        return self.spark.sparkContext.broadcast([dataset_version, self.regions_cache])

    def pre_transform(self, errors, warnings) -> None:
        fix_date_udf = udf(fix_date, TimestampType())
        df_derived = self.df.withColumn(
            "default_datetime",
            fix_date_udf("default_datetime"),
        )

        fix_nca_cassowary_udf = udf(fix_cassowary, StringType())
        generalise_lat_udf = udf(generalise_latitude_for_udf, FloatType())
        generalise_long_udf = udf(generalise_longitude_for_udf, FloatType())
        df_derived = (
            df_derived.withColumn(
                "gen_lat",
                generalise_lat_udf("generalisation", "latdecimal", "longdecimal"),
            )
            .withColumn(
                "gen_long",
                generalise_long_udf("generalisation", "latdecimal", "longdecimal"),
            )
            .withColumn(
                "nca_status",
                fix_nca_cassowary_udf("nca_status", "latdecimal", "will_species"),
            )
            .withColumn(
                "epbc_status",
                when(col("will_species") == "Casuarius casuarius", lit("E")).otherwise(
                    col("epbc_status")
                ),
            )
            .withColumn(
                "significant",
                when(col("will_species") == "Casuarius casuarius", lit("Y")).otherwise(
                    col("significant")
                ),
            )
            .withColumn(
                "confidential",
                when(col("will_species") == "Casuarius casuarius", lit("N")).otherwise(
                    col("confidential")
                ),
            )
            .withColumn(
                "endemicity",
                when(col("will_species") == "Casuarius casuarius", lit("Q")).otherwise(
                    col("endemicity")
                ),
            )
            .withColumn(
                "scientificname",
                when(
                    col("will_species") == "Casuarius casuarius", lit("Casuarius casuarius")
                ).otherwise(col("scientificname")),
            )
            .withColumn(
                "authorship",
                when(col("will_species") == "Casuarius casuarius", lit("Linnaeus, 1758")).otherwise(
                    col("authorship")
                ),
            )
            .withColumn(
                "rank",
                when(col("will_species") == "Casuarius casuarius", lit("species")).otherwise(
                    col("rank")
                ),
            )
            .withColumn(
                "alaid",
                when(
                    col("will_species") == "Casuarius casuarius",
                    lit(
                        "https://biodiversity.org.au/afd/taxa/5fc1b9fe-ccdd-4c3a-adfc-6f6e5c5c396b"
                    ),
                ).otherwise(col("alaid")),
            )
        )

        # df_derived.select("authorship").show()

        df_derived = (
            df_derived.withColumn(
                "scientificname",
                when(
                    col("will_species") == "Gavicalis chrysops", lit("Caligavis chrysops barroni")
                ).otherwise(col("scientificname")),
            )
            .withColumn(
                "authorship",
                when(
                    col("will_species") == "Gavicalis chrysops", lit("Mathews, G.M. 1912")
                ).otherwise(col("authorship")),
            )
            .withColumn(
                "genus",
                when(col("will_species") == "Gavicalis chrysops", lit("Caligavis")).otherwise(
                    col("genus")
                ),
            )
            .withColumn(
                "rank",
                when(col("will_species") == "Gavicalis chrysops", lit("subspecies")).otherwise(
                    col("rank")
                ),
            )
            .withColumn(
                "alaid",
                when(
                    col("will_species") == "Gavicalis chrysops",
                    lit(
                        "https://biodiversity.org.au/afd/taxa/7f02401c-31f7-4a4a-a28c-4c4a4a4edecc"
                    ),
                ).otherwise(col("alaid")),
            )
        )

        # df_derived.select("authorship").show()

        df_derived = (
            df_derived.withColumn(
                "scientificname",
                when(
                    col("will_species") == "Phylidonyris nigra",
                    lit("Phylidonyris (Meliornis) niger niger"),
                ).otherwise(col("scientificname")),
            )
            .withColumn(
                "authorship",
                when(col("will_species") == "Phylidonyris nigra", lit("Bechstein, 1811")).otherwise(
                    col("authorship")
                ),
            )
            .withColumn(
                "genus",
                when(col("will_species") == "Phylidonyris nigra", lit("Phylidonyris")).otherwise(
                    col("genus")
                ),
            )
            .withColumn(
                "rank",
                when(col("will_species") == "Phylidonyris nigra", lit("subspecies")).otherwise(
                    col("rank")
                ),
            )
            .withColumn(
                "alaid",
                when(
                    col("will_species") == "Phylidonyris nigra",
                    lit(
                        "https://biodiversity.org.au/afd/taxa/5d426833-a191-45ba-9a3c-d59c5f42ca44"
                    ),
                ).otherwise(col("alaid")),
            )
        )

        df_derived = (
            df_derived.withColumn(
                "scientificname",
                when(
                    col("will_species") == "Heteromyias albispecularis",
                    lit("Heteromyias cinereifrons"),
                ).otherwise(col("scientificname")),
            )
            .withColumn(
                "authorship",
                when(
                    col("will_species") == "Heteromyias albispecularis", lit("E.P. Ramsay, 1876")
                ).otherwise(col("authorship")),
            )
            .withColumn(
                "genus",
                when(
                    col("will_species") == "Heteromyias albispecularis", lit("Heteromyias")
                ).otherwise(col("genus")),
            )
            .withColumn(
                "rank",
                when(col("will_species") == "Heteromyias albispecularis", lit("species")).otherwise(
                    col("rank")
                ),
            )
            .withColumn(
                "alaid",
                when(
                    col("will_species") == "Heteromyias albispecularis",
                    lit(
                        "https://biodiversity.org.au/afd/taxa/a4e8e748-2e70-42a6-9bfb-3e0816bf026c"
                    ),
                ).otherwise(col("alaid")),
            )
        )

        df_derived = (
            df_derived.withColumn(
                "scientificname",
                when(
                    col("will_species") == "Carternornis leucotis", lit("Carterornis leucotis")
                ).otherwise(col("scientificname")),
            )
            .withColumn(
                "authorship",
                when(col("will_species") == "Carternornis leucotis", lit("Gould, 1850")).otherwise(
                    col("authorship")
                ),
            )
            .withColumn(
                "genus",
                when(col("will_species") == "Carternornis leucotis", lit("Carterornis")).otherwise(
                    col("genus")
                ),
            )
            .withColumn(
                "rank",
                when(col("will_species") == "Carternornis leucotis", lit("species")).otherwise(
                    col("rank")
                ),
            )
            .withColumn(
                "alaid",
                when(
                    col("will_species") == "Carternornis leucotis",
                    lit(
                        "https://biodiversity.org.au/afd/taxa/c8798827-cbea-4292-9be7-927143d4ede5"
                    ),
                ).otherwise(col("alaid")),
            )
        )

        df_derived = (
            df_derived.withColumn(
                "scientificname",
                when(
                    col("will_species") == "Lophorina victoriae",
                    lit("Ptiloris (Ptiloris) victoriae"),
                ).otherwise(col("scientificname")),
            )
            .withColumn(
                "authorship",
                when(col("will_species") == "Lophorina victoriae", lit("Gould, 1850")).otherwise(
                    col("authorship")
                ),
            )
            .withColumn(
                "genus",
                when(col("will_species") == "Lophorina victoriae", lit("Ptiloris")).otherwise(
                    col("genus")
                ),
            )
            .withColumn(
                "rank",
                when(col("will_species") == "Lophorina victoriae", lit("species")).otherwise(
                    col("rank")
                ),
            )
            .withColumn(
                "alaid",
                when(
                    col("will_species") == "Lophorina victoriae",
                    lit(
                        "https://biodiversity.org.au/afd/taxa/2a883db3-2a71-4e90-a23a-73e9ea68616c"
                    ),
                ).otherwise(col("alaid")),
            )
        )

        df_derived = (
            df_derived.withColumn(
                "scientificname",
                when(
                    col("will_species") == "Morelia amethistina", lit("Simalia kinghorni")
                ).otherwise(col("scientificname")),
            )
            .withColumn(
                "authorship",
                when(col("will_species") == "Morelia amethistina", lit("Stull, 1933")).otherwise(
                    col("authorship")
                ),
            )
            .withColumn(
                "genus",
                when(col("will_species") == "Morelia amethistina", lit("Simalia")).otherwise(
                    col("genus")
                ),
            )
            .withColumn(
                "rank",
                when(col("will_species") == "Morelia amethistina", lit("species")).otherwise(
                    col("rank")
                ),
            )
            .withColumn(
                "alaid",
                when(
                    col("will_species") == "Morelia amethistina",
                    lit(
                        "https://biodiversity.org.au/afd/taxa/0dadb267-e6c0-4174-b594-1164c33630ed"
                    ),
                ).otherwise(col("alaid")),
            )
        )

        df_derived = (
            df_derived.withColumn(
                "scientificname",
                when(col("will_species") == "Saproscincus spp.", lit("Saproscincus sp.")).otherwise(
                    col("scientificname")
                ),
            )
            .withColumn(
                "genus",
                when(col("will_species") == "Saproscincus spp.", lit("Saproscincus")).otherwise(
                    col("genus")
                ),
            )
            .withColumn(
                "alaid",
                when(
                    col("will_species") == "Saproscincus spp.",
                    lit(
                        "https://biodiversity.org.au/afd/taxa/39db93d8-0cc7-4b43-b24d-2b586c768f9a"
                    ),
                ).otherwise(col("alaid")),
            )
        )

        df_derived = (
            df_derived.withColumn(
                "scientificname",
                when(col("will_species") == "Lygisaurus spp.", lit("Lygisaurus sp.")).otherwise(
                    col("scientificname")
                ),
            )
            .withColumn(
                "genus",
                when(col("will_species") == "Lygisaurus spp.", lit("Lygisaurus")).otherwise(
                    col("genus")
                ),
            )
            .withColumn(
                "alaid",
                when(
                    col("will_species") == "Lygisaurus spp.",
                    lit(
                        "https://biodiversity.org.au/afd/taxa/4b5abc12-3bc9-49d7-b840-4d40eb24688e"
                    ),
                ).otherwise(col("alaid")),
            )
        )

        df_derived = (
            df_derived.withColumn(
                "scientificname",
                when(
                    col("will_species") == "Chaerophon jobensis", lit("Chaerephon jobensis")
                ).otherwise(col("scientificname")),
            )
            .withColumn(
                "authorship",
                when(col("will_species") == "Chaerophon jobensis", lit("Miller, 1902")).otherwise(
                    col("authorship")
                ),
            )
            .withColumn(
                "genus",
                when(col("will_species") == "Chaerophon jobensis", lit("Chaerephon")).otherwise(
                    col("genus")
                ),
            )
            .withColumn(
                "rank",
                when(col("will_species") == "Chaerophon jobensis", lit("species")).otherwise(
                    col("rank")
                ),
            )
            .withColumn(
                "alaid",
                when(
                    col("will_species") == "Chaerophon jobensis",
                    lit(
                        "https://biodiversity.org.au/afd/taxa/1806b689-e894-4cb4-89c5-ea18a1a64a9f"
                    ),
                ).otherwise(col("alaid")),
            )
        )

        df_derived = (
            df_derived.withColumn(
                "scientificname",
                when(col("will_species") == "Certhionyx niger", lit("Sugomel niger")).otherwise(
                    col("scientificname")
                ),
            )
            .withColumn(
                "authorship",
                when(col("will_species") == "Certhionyx niger", lit("Gould, 1838")).otherwise(
                    col("authorship")
                ),
            )
            .withColumn(
                "genus",
                when(col("will_species") == "Certhionyx niger", lit("Sugomel")).otherwise(
                    col("genus")
                ),
            )
            .withColumn(
                "rank",
                when(col("will_species") == "Certhionyx niger", lit("species")).otherwise(
                    col("rank")
                ),
            )
            .withColumn(
                "alaid",
                when(
                    col("will_species") == "Certhionyx niger",
                    lit(
                        "https://biodiversity.org.au/afd/taxa/b32a2ec6-315c-48cf-84b3-4898e39f4b57"
                    ),
                ).otherwise(col("alaid")),
            )
        )
        df_derived = df_derived.replace("NA", None)
        # df_derived.show()

        df_derived = df_derived.withColumn(
            "HASH",
            concat(
                col("latdecimal").cast("string"),
                lit("|"),
                col("longdecimal").cast("string"),
            ),
        )

        collected = (
            df_derived.select("HASH").orderBy("unique_id", ascending=True).toPandas()["HASH"]
        )

        conn = pg.connect(
            host=self.db_host,
            port=self.db_port,
            dbname="ultimate_feature_of_interest",
            user=self.db_username,
            password=self.db_password,
        )

        for i in range(0, len(collected)):
            # print(i)
            hash = collected[i].split("|")
            lat = hash[0]
            long = hash[1]

            # print(f"{i} {lat} {long} {collected[i]}")
            if collected[i] not in self.regions_cache:
                # Calculate regions from TERN PostGIS
                regions = []
                src_cursor = conn.cursor()
                for ufoi_key in Config.ufoi_list.keys():
                    src_cursor.execute(
                        "select %(value)s from %(table_name)s where ST_Intersects(geom, ST_SetSRID(ST_MakePoint(%(lon)s, %(lat)s), 3577))",
                        {
                            "table_name": AsIs(ufoi_key),
                            "value": AsIs(Config.ufoi_list[ufoi_key][1]),
                            "lon": long,
                            "lat": lat,
                        },
                    )
                    result = src_cursor.fetchone()
                    if result is not None:
                        region_parent = Config.ufoi_list[ufoi_key][0]
                        regions.append(f"{region_parent}{result[0]}")

                self.regions_cache[collected[i]] = regions
                src_cursor.close()

        with open("regions_cache.pkl", "wb") as cache:
            # with open(""C:\\Users\\uqjsanc2\\PycharmProjects\\etl-spark-williams\\regions_cache.pkl"", "wb") as cache:
            try:
                pickle.dump(self.regions_cache, cache)
            except Exception as e:
                print(str(e))

        df_derived.printSchema()

        self.table = "tern_{}".format(self.table)
        save_dataframe_as_jdbc_table(
            df_derived,
            get_postgres_url(
                self.db_host,
                self.db_port,
                self.db_name,
                self.db_username,
                self.db_password,
            ),
            self.dataset,
            self.table,
        )
        self.df = df_derived

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        regions_cache = lookup.value[1]

        ns = Namespace(namespace_url)
        # In order to loop over the "rows" iterator 2 times, we need to create a copy (as the iterator
        # is exhausted after the first loop).
        rows, rows_cloned = itertools.tee(rows)
        g = generate_rdf_graph(
            rows,
            dataset,
            ns,
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        dataset_uri = RDFDataset.generate_uri(ns)
        g += RDFDataset(
            uri=dataset_uri,
            title=Literal("Williams Wet Tropics Vertebrate database"),
            description=Literal(
                """<p>This database contains occurrence data for vertebrates across the
            Australian Wet Tropics. Species occurrence point data has been collected during
            field intensive surveys as well as collated from the literature and institutional
            database using a variety of sampling methods. The records are divided into two tables:
             Misc_records and STD_records. The first contains records collated from the literature.
             The latter is a collection of standardized surveys conducted by Steve E. Williams (JCU).
             </p> <p> All occurrences were vetted for positional and taxonomic accuracy, and for sensitivity
             at the state and national levels. Sensitive species records are withheld or have their
             location generalised following state-level rules for processing these records. </p>"""
            ),
            issued=Literal(lookup.value[0], datatype=XSD.date),
            # creator=URIRef(
            #     "https://w3id.org/tern/resources/3f2c7882-111d-4bc1-ad4e-a902a40867ee"
            # ),
            citation=Literal(
                "Williams, S. (2021): Williams Wet Tropics Vertebrate database. Version 1.0.0. Terrestrial Ecosystem Research Network (TERN). (Dataset)."
            ),
            # publisher=URIRef(
            #     "https://w3id.org/tern/resources/a083902d-d821-41be-b663-1d7cb33eea66"
            # ),
        ).g

        mappings = get_mappings(vocabulary_mappings.value, errors)

        for row in rows_cloned:
            for column_name, column_mapping in mappings.items():
                if column_mapping["exclude"] is not None and column_mapping["exclude"] == False:
                    if column_mapping["record_type"] == RECORD_TYPE_OBSERVATION:
                        if row[column_mapping["db_column"]]:
                            g = create_point_and_regions(
                                column_mapping["db_table"],
                                column_mapping["db_column"],
                                g,
                                ns,
                                regions_cache,
                                row,
                            )

            foi_uri = FeatureOfInterest.generate_uri(ns, "animal_occurr_id", row)

            # start_date = datetime.strptime(
            #     row["default_datetime"].strftime("%Y-%m-%d %H:%M:%S"),
            #     "%Y-%m-%d %H:%M:%S",
            # )
            # survey_uri = Survey.generate_uri(Namespace(namespace_url), row[SURVEY_ID])
            # g += Survey(
            #     uri=survey_uri,
            #     identifier=Literal(start_date.strftime("%Y%m%d"), datatype=XSD.string),
            #     in_dataset=dataset_uri,
            #     started_at_time=Literal(row["default_datetime"], datatype=XSD.dateTime),
            # ).g

            species_name = row["scientificname"]
            if species_name:
                taxon = Taxon(
                    uri=URIRef(row["alaid"]),
                    label=Literal(row["scientificname"]),
                    taxon_id=Literal(row["alaid"]),
                    taxon_rank=Literal(row["rank"]),
                    kingdom=Literal(row["kingdom"]),
                    phylum=Literal(row["phylum"]),
                    _class=Literal(row["class"]),
                    order=Literal(row["order"]),
                    family=Literal(row["family"]),
                    genus=Literal(row["genus"]) if row["genus"] is not None else None,
                    scientific_name=Literal(row["scientificname"]),
                    scientific_name_authorship=Literal(row["authorship"]),
                    vernacular_name=(
                        Literal(row["common_name"]) if row["common_name"] is not None else None
                    ),
                )

                obs_uri = Observation.generate_uri(
                    ns,
                    "tern_std_misc_records_status_taxonomy",
                    "taxon",
                    row[UNIQUE_ID],
                )
                tags = f"""taxon,animal occurrence,opportunistic,{species_name}"""
                g += Observation(
                    uri=obs_uri,
                    feature_of_interest=foi_uri,
                    in_dataset=dataset_uri,
                    observed_property=URIRef(
                        "http://linked.data.gov.au/def/tern-cv/70646576-6dc7-4bc5-a9d8-c4c366850df0"
                    ),
                    used_procedure=URIRef(
                        "http://linked.data.gov.au/def/tern-cv/de94b8fd-5c66-4338-9652-535c8610458f"
                    ),
                    has_result=taxon,
                    has_simple_result=Literal(species_name, datatype=XSD.string),
                    phenomenon_time=Instant(
                        uri=generate_underscore_uri(ns),
                        datetime=Literal(row["default_datetime"], datatype=XSD.dateTime),
                    ),
                    result_datetime=Literal(row["default_datetime"], datatype=XSD.dateTime),
                    has_site=None,
                    has_site_visit=None,
                    ecoplots_tags=Literal(tags, datatype=XSD.string),
                ).g
                g = create_point_and_regions(
                    "tern_std_misc_records_status_taxonomy",
                    "taxon",
                    g,
                    ns,
                    regions_cache,
                    row,
                    obs_uri,
                )

                # if row["nca_status"]:
                #     cs_obs_uri = Observation.generate_uri(
                #         ns,
                #         "tern_std_records_status_taxonomy",
                #         "nca_status",
                #         row[UNIQUE_ID],
                #     )
                #     g += Attribute(
                #         uri=Attribute.generate_uri(
                #             Namespace(namespace_url),
                #             "tern_std_records_status_taxonomy",
                #             "jurisdiction",
                #             row[UNIQUE_ID],
                #         ),
                #         in_dataset=dataset_uri,
                #         is_attribute_of=cs_obs_uri,
                #         attribute=URIRef(
                #             "http://linked.data.gov.au/def/tern-cv/755b1456-b76f-4d54-8690-10e41e25c5a7"
                #         ),
                #         has_value=IRI(
                #             uri=generate_underscore_uri(ns),
                #             value=URIRef(
                #                 "http://linked.data.gov.au/def/tern-cv/c7fb8405-3906-4056-a004-b6ce44d018c5"
                #             ),
                #         ),
                #         has_simple_value=URIRef(
                #             "http://linked.data.gov.au/def/tern-cv/c7fb8405-3906-4056-a004-b6ce44d018c5"
                #         ),
                #     ).g

        post_transform(g, ontology.value, table_name)
