import datetime
import time
from typing import Iterable

from pyspark import Accumulator, Broadcast
from rdflib import RDF, XSD, Graph
from rdflib.term import Literal, URIRef
from spark_etl_utils import Transform, write_to_disk
from spark_etl_utils.custom_accumulators import add_error
from spark_etl_utils.rdf import (
    UNIQUE_ID,
    generate_underscore_uri,
    get_vocabulary_concept,
    parse_vocab_from_url,
)
from spark_etl_utils.rdf.inference import infer_subclasses
from spark_etl_utils.rdf.models import Observation, Point
from tern_rdf.namespace_bindings import AUSPLOTS_BINDINGS, GEOSPARQL, TERN, TernRdf

from config import Config


def perform_transform(
    class_instance: Transform,
    transform,
    dataset: str,
    namespace_url: str,
    table_name: str,
    vocabulary_mappings: Broadcast,
    vocabulary_graph: Broadcast,
    errors: Accumulator,
    warnings: Accumulator,
    lookup: Broadcast,
    ontology: Broadcast,
) -> None:
    """
    Perform a Spark distributed job on a DataFrame using the foreach method.

    :param class_instance: Transform class instance
    :param transform: foreach's transform function
    :param table_name: Table name for logging
    :param vocabulary_graph: RDF data as a Broadcast
    :param errors: Errors Accumulator
    :param lookup: Lookup Broadcast
    """

    # Split the DataFrame into logical chunks
    # TODO: Determine best way to repartition DataFrame.
    if not Config.run_on_single_process:
        if (
            class_instance.table == "tern_std_records_status_taxonomy"
            or class_instance.table == "tern_misc_records_status_taxonomy"
        ):
            class_instance.df = class_instance.df.repartition(Config.CPU_COUNT * 2)
        else:
            class_instance.df = class_instance.df.repartition(Config.CPU_COUNT)
    else:
        class_instance.df = class_instance.df.repartition(1)

    # print("Number of partitions: {}".format(class_instance.df.rdd.getNumPartitions()))
    # print("Partitioner: {}".format(class_instance.df.rdd.partitioner))
    # print("Partitions structure: {}".format(class_instance.df.rdd.glom().collect()))

    # Perform transformation
    class_instance.df.foreachPartition(
        lambda rows_in_partition: transform(
            rows_in_partition,
            dataset,
            namespace_url,
            table_name,
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
            lookup,
            ontology,
        )
    )
    print(
        "Length of {} DataFrame: {}".format(
            class_instance.table, class_instance.df.count()
        )
    )

    if errors.value:
        print("Errors for table {}:".format(class_instance.table), len(errors.value))
        for i, item in enumerate(errors.value):
            print(i, item)
    else:
        print("No errors for table {}.".format(class_instance.table))


def _fetch_ontology(url: str) -> Graph:
    print("Loading TERN ontology into graph...")
    retry = 0
    max_retries = 5
    while retry < max_retries:
        try:
            ontology_graph = parse_vocab_from_url(
                TernRdf.Graph([AUSPLOTS_BINDINGS]), url
            )
            return ontology_graph
        except Exception as e:
            retry += 1
            print("Failed loading TERN ontology RETRY: {}".format(retry))
            if retry < max_retries:
                time.sleep(60)
            else:
                raise e


def load_ontology(ontology_urls: Iterable):
    g = Graph()
    for url in ontology_urls:
        g += _fetch_ontology(url)

    return g


def infer(g: Graph, ontology_graph: Graph):
    """
    Execute inferring rules in the Graph
    """
    infer_subclasses(g, ontology_graph, TERN.Observation)
    infer_subclasses(g, ontology_graph, TERN.FeatureOfInterest)
    # infer_subclasses(g, ontology_graph, TERN.Site)
    infer_subclasses(g, ontology_graph, TERN.Value)
    # infer_subclasses(g, ontology_graph, TERN_LOC.Geometry)


def post_transform(g: Graph, ontology_graph: Graph, table_name: str):
    infer(g, ontology_graph)
    # shacl_validator(g, plot_ontology_shacl, vocabs, table_name, errors)
    write_to_disk(
        g, "", Config.OUTPUT_DIR, table_name, rdf_format=Config.OUTPUT_RDF_FORMAT
    )


def get_conservation_status(vocabs, value, errors):
    result_uri = get_vocabulary_concept(
        vocabs,
        URIRef(
            "http://linked.data.gov.au/def/tern-cv/84a3b2fd-0919-4afe-a4a2-7b2c643ebbea"
        ),
        URIRef("http://www.w3.org/2004/02/skos/core#notation"),
        value,
    )
    if result_uri:
        return URIRef(result_uri)
    else:
        add_error(
            errors, "Concept was not found for {}: {}".format("nca_status", value)
        )
        return None


def fix_date(date_time):
    if date_time != "NA":
        return datetime.datetime.strptime(date_time, "%Y-%m-%dT%H:%M:%SZ")
    else:
        return datetime.datetime.fromtimestamp(0)


def fix_cassowary(nca_status, lat, species):
    if species == "Casuarius casuarius":
        return "V" if lat > -13.864 else "E"
    else:
        return nca_status


def create_point_and_regions(
    db_table, db_column, g, ns, regions_cache, row, obs_uri=None
):
    lat = row["gen_lat"]
    long = row["gen_long"]
    point_uri = URIRef(generate_underscore_uri(ns))
    point = Point(
        uri=point_uri,
        as_wkt=Literal(
            "POINT({} {})".format(round(long, 8), round(lat, 8)),
            datatype=GEOSPARQL.wktLiteral,
        ),
        latitude=Literal(lat, datatype=XSD.decimal),
        longitude=Literal(long, datatype=XSD.decimal),
    )
    g += point.g

    if not obs_uri:
        obs_uri = Observation.generate_uri(
            ns,
            db_table,
            db_column,
            row[UNIQUE_ID],
        )

    # print(obs_uri)

    g.add(
        (
            obs_uri,
            GEOSPARQL.hasGeometry,
            point_uri,
        )
    )
    g.add(
        (
            point_uri,
            RDF.type,
            GEOSPARQL.Geometry,
        )
    )

    if row["HASH"] in regions_cache:
        regions = regions_cache[row["HASH"]]
        for reg in regions:
            g.add(
                (
                    obs_uri,
                    GEOSPARQL.sfWithin,
                    URIRef(reg),
                )
            )

    return g
