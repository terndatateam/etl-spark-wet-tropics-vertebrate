import os

import numexpr
from tern_rdf.namespace_bindings import WET_TROPICS_VERT_DATASET

try:
    from dotenv import load_dotenv

    load_dotenv()
except Exception:
    pass


class Config:
    APP_DIR = os.path.dirname(os.path.realpath(__file__))
    OUTPUT_DIR = "output"
    OUTPUT_RDF_FORMAT = "turtle"

    # Sets a name for the application, which will be shown in the Spark web UI.
    # If no application name is set, a randomly generated name will be used.
    APP_NAME = "WILLIAMS SPARK-ETL"

    DATASET = "williams_wet_tropics_vertebrate_dataset"
    DATASET_NAMESPACE = WET_TROPICS_VERT_DATASET

    METADATA_URI = "5eb7332c-b610-4549-b540-077415efaf89"

    # Sets the Spark master URL to connect to, such as "local" to run locally, "local[4]" to run locally with 4 cores,
    # or "spark://master:7077" to run on a Spark standalone cluster.
    CPU_COUNT = numexpr.detect_number_of_cores()
    print("Executing ETL with {} cores/threads".format(CPU_COUNT))
    APP_MASTER = "local[{0}]".format(CPU_COUNT)

    # Select the tables to process commenting and uncommenting them
    tables = [
        "std_records_status_taxonomy",
        # "misc_records_status_taxonomy",
    ]

    ufoi_list = {
        "local_government_areas_2011": [
            "http://linked.data.gov.au/dataset/local-gov-areas-2011/",
            "lga_code11",
        ],
        "capad_2018_terrestrial": [
            "http://linked.data.gov.au/dataset/capad-2018-terrestrial/",
            "pa_pid",
        ],
        "ibra7_regions": ["http://linked.data.gov.au/dataset/bioregion/", "reg_code_7"],
        "ibra7_subregions": [
            "http://linked.data.gov.au/dataset/bioregion/",
            "sub_code_7",
        ],
        "nrm_regions": ["http://linked.data.gov.au/dataset/nrm-2017/", "nrm_id"],
        "states_territories": [
            "http://linked.data.gov.au/dataset/asgs2016/stateorterritory/",
            "state_code",
        ],
        "wwf_terr_ecoregions": [
            "http://linked.data.gov.au/dataset/wwf-terr-ecoregions/",
            "objectid",
        ],
    }

    # Force the execution to re-download the Taxa files and re-index them.
    # This may take a long time
    force_taxa_index = False

    # APNI and APC urls (for taxa tables)
    APC = "http://linked.data.gov.au/dataset/apc"

    # Choose the currently accepted national taxonomy needed for the project
    taxas = [
        "APNI/APC",
        "AusMoss",
        # 'Fungi',
        # 'Lichen'
    ]

    HERBARIUM_FILES = "taxa_files"

    # Source for vocabularies and ontologies
    VOCABS = [
        "https://graphdb.tern.org.au/repositories/tern_vocabs_core/statements",
    ]

    ONTOLOGIES = [
        "https://raw.githubusercontent.com/ternaustralia/ontology_tern/master/docs/tern.ttl",
        "https://w3id.org/tern/ontologies/loc.ttl",
    ]

    RELOAD_MAPPINGS = True
    MAPPING_CSV = "https://docs.google.com/spreadsheets/d/1Zon-3em4OLUoPwCm4KAK2I_VRmDsu8zt4Fm6zYsNGKc/export?format=csv&gid=915058859"

    # ---------- SHACL Validation --------------------------------------------------------------------------------------

    # SHACL Shape file path
    SHACL_SHAPE = "./shacl/plot-ontology-shacl.ttl"

    # --------- DEBUG SETTINGS -----------------------------------------------------------------------------------------

    # Set to true to execute only in one core (even if the spark job has started with more cores)
    run_on_single_process = False

    # Setting this variable to True will filter the source dataset with the indicated IDs
    DEBUG = False
