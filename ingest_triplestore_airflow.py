import argparse
import datetime
import errno
import gzip
import os
import shutil
import tarfile
import time
from zipfile import ZIP_DEFLATED, ZipFile

import requests
from jumpssh import RunCmdError, SSHSession
from requests.auth import HTTPBasicAuth
from scp import SCPClient
from SPARQLWrapper import SPARQLWrapper

from config import Config

TRIPLESTORE_PROXY_GATE = "bastion.tern.org.au"
TRIPLESTORE_SERVER_USER = "ec2-user"
# SSH_KEY = '/home/ec2-user/.ssh/tern_key'
SSH_KEY = "/home/ubuntu/.ssh/ecoplots-etl"
# SSH_KEY = "C://ansible.pem"

VIRTUOSO_DATA_FOLDER = "/home/ec2-user/virtuoso-data"
ANZOGRAPH_DATA_FOLDER = "/home/ec2-user/azg-data"
GRAPHDB_DATA_FOLDER = "/graphdb-import"
LOAD_DATA_VIRTUOSO_SH = "load_data_virtuoso.sh"
DELETE_GRAPH_VIRTUOSO_SH = "delete_graph_virtuoso.sh"
DUMP_DATA_VIRTUOSO_SH = "dump_virtuoso.sh"


def ingest_virtuoso(triplestore_ip, dataset):
    path = os.path.join(Config.APP_DIR, Config.OUTPUT_DIR)
    # path = "/indexer/ausplots/output"
    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if file[-4:] == ".ttl":
                files.append((r, file))

    print("Files:", files)
    print("Number of files:", len(files))

    historical_dir = path + time.strftime("/%Y%m%d")
    errors_dir = historical_dir + "/errors"
    if not os.path.exists(historical_dir):
        os.makedirs(historical_dir)
    if not os.path.exists(errors_dir):
        os.makedirs(errors_dir)

    start_time = time.time()

    gateway_session = SSHSession(
        triplestore_ip, TRIPLESTORE_SERVER_USER, private_key_file=SSH_KEY
    ).open()
    scp = SCPClient(gateway_session.ssh_transport)

    print("Uploading Turtle files one by one to Virtuoso server (SCP)")

    i = 1
    for r, f in files:
        gzip_filename = f"{r}/{dataset}_{f}.gz"
        with open((os.path.join(r, f)), "rb") as f_in:
            with gzip.open(gzip_filename, "wb") as f_out:
                shutil.copyfileobj(f_in, f_out)
        print(f"{i}/{len(files)}")
        i += 1

        try:
            scp.put(gzip_filename, remote_path=f"{VIRTUOSO_DATA_FOLDER}/data")
            silentremove(gzip_filename)

        except Exception as err:
            print("Error uploading file to Virtuoso server: ", err)
            os.rename(gzip_filename, errors_dir + "/" + os.path.basename(gzip_filename))

    print("Files uploaded in {:.2f} seconds".format(time.time() - start_time))

    start_time = time.time()
    try:
        ssh_cmd = (
            f"sudo podman exec virtuoso /virtuoso-data/delete_graph.sh '{dataset}'"
        )
        # gateway_session.ssh_client.exec_command(ssh_cmd)
        print(gateway_session.get_cmd_output(ssh_cmd))
        # time.sleep(10)
        while True:
            try:
                ssh_cmd = f"test -f {VIRTUOSO_DATA_FOLDER}/{dataset}_DELETE_FINISHED"
                # gateway_session.ssh_client.exec_command(ssh_cmd)
                print(gateway_session.get_cmd_output(ssh_cmd))
                try:
                    ssh_cmd = f"rm -f {VIRTUOSO_DATA_FOLDER}/{dataset}_DELETE_FINISHED"
                    # gateway_session.ssh_client.exec_command(ssh_cmd)
                    print(gateway_session.get_cmd_output(ssh_cmd))
                except Exception as e:
                    print(e)
                break
            except RunCmdError as err:
                print(f"Deleting {dataset} data...")
                time.sleep(30)
                print(err)
    except Exception as e:
        print(e)
    print(f"{dataset} graph deleted in {time.time() - start_time:.2f} seconds")

    try:
        ssh_cmd = f"sh virtuoso-data/load_vocabs_graphDB.sh"
        # gateway_session.ssh_client.exec_command(ssh_cmd)
        print(gateway_session.get_cmd_output(ssh_cmd))
        # time.sleep(10)
        # while True:
        #    try:
        #        ssh_cmd = f"test -f {VIRTUOSO_DATA_FOLDER}/VOCABS_FINISHED"
        #        #gateway_session.ssh_client.exec_command(ssh_cmd)
        #        print(gateway_session.get_cmd_output(ssh_cmd))
        #        try:
        #            ssh_cmd = f"rm -f {VIRTUOSO_DATA_FOLDER}/VOCABS_FINISHED"
        #            #gateway_session.ssh_client.exec_command(ssh_cmd)
        #            print(gateway_session.get_cmd_output(ssh_cmd))
        #        except Exception as e:
        #            print(e)
        #        break;
        #    except RunCmdError as err:
        #        print(f"Loading vocabs data...")
        #        time.sleep(30)
        #        print(err)
    except Exception as e:
        print(e)
    print(f"Vocabs loaded into Virtuoso")

    start_time = time.time()
    try:
        ssh_cmd = f"sudo podman exec virtuoso /virtuoso-data/load_graph.sh '/virtuoso-data' '{dataset}'"
        # gateway_session.ssh_client.exec_command(ssh_cmd)
        print(gateway_session.get_cmd_output(ssh_cmd))
        # time.sleep(10)
        while True:
            try:
                ssh_cmd = f"test -f {VIRTUOSO_DATA_FOLDER}/{dataset}_UPLOAD_FINISHED"
                # gateway_session.ssh_client.exec_command(ssh_cmd)
                print(gateway_session.get_cmd_output(ssh_cmd))
                try:
                    ssh_cmd = f"rm -f {VIRTUOSO_DATA_FOLDER}/{dataset}_UPLOAD_FINISHED"
                    # gateway_session.ssh_client.exec_command(ssh_cmd)
                    print(gateway_session.get_cmd_output(ssh_cmd))
                except Exception as e:
                    print(e)
                break
            except RunCmdError as err:
                print(f"Loading {dataset} data...")
                time.sleep(30)
                print(err)
    except Exception as e:
        print(e)
    print(f"Data loaded into Virtuoso in {time.time() - start_time:.2f} seconds")


def cli():
    parser = argparse.ArgumentParser(description="Ingest RDF data into triplestores")
    subparsers = parser.add_subparsers(help="Triplestore")

    # Virtuoso
    virtuoso_parser = subparsers.add_parser(
        "virtuoso", help="Ingest into Virtuoso open source"
    )
    virtuoso_parser.add_argument(
        "--triplestore", action="store", default="virtuoso", help=argparse.SUPPRESS
    )
    virtuoso_parser.add_argument(
        "--ip", action="store", dest="ip", required=True, help="Virtuoso private IP"
    )
    virtuoso_parser.add_argument(
        "--dataset", action="store", dest="dataset", required=True, help="Dataset name"
    )

    # Anzograph
    anzo_parser = subparsers.add_parser("anzograph", help="Ingest into Anzograph")
    anzo_parser.add_argument(
        "--triplestore", action="store", default="anzograph", help=argparse.SUPPRESS
    )
    anzo_parser.add_argument(
        "--ip", action="store", dest="ip", required=True, help="Anzograph private IP"
    )
    anzo_parser.add_argument(
        "--sparql_endpoint",
        action="store",
        dest="sparql_endpoint",
        required=True,
        help="Anzograph SPARQL endpoint",
    )
    anzo_parser.add_argument(
        "--user", action="store", dest="user", required=True, help="Anzograph user"
    )
    anzo_parser.add_argument(
        "--password",
        action="store",
        dest="password",
        required=True,
        help="Anzograph password",
    )
    anzo_parser.add_argument(
        "--dataset", action="store", dest="dataset", required=True, help="Dataset name"
    )

    # GraphDB
    graphdb_parser = subparsers.add_parser("graphdb", help="Ingest into GraphDB")
    graphdb_parser.add_argument(
        "--triplestore", action="store", default="graphdb", help=argparse.SUPPRESS
    )
    graphdb_parser.add_argument(
        "--ip", action="store", dest="ip", required=True, help="GraphDB private IP"
    )
    graphdb_parser.add_argument(
        "--repository",
        action="store",
        dest="repository",
        required=True,
        help="GraphDB repository",
    )
    graphdb_parser.add_argument(
        "--user", action="store", dest="user", required=True, help="GraphDB user"
    )
    graphdb_parser.add_argument(
        "--password",
        action="store",
        dest="password",
        required=True,
        help="GraphDB password",
    )
    graphdb_parser.add_argument(
        "--dataset", action="store", dest="dataset", required=True, help="Dataset name"
    )

    return parser.parse_args()


def ingest_graphdb(triplestore_ip, repository, user, password, dataset):
    # path = os.path.join(Config.APP_DIR, Config.OUTPUT_DIR)
    path = "/indexer/ausplots/output"
    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if file[-4:] == ".ttl":
                files.append(os.path.join(r, file))

    print("Files:", files)
    print("Number of files:", len(files))

    loading_date = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    historical_dir = path + "/" + loading_date
    if not os.path.exists(historical_dir):
        os.makedirs(historical_dir)

    start_time = time.time()

    zip_filename = "{dataset}_{time}.ttl.zip".format(dataset=dataset, time=loading_date)
    with ZipFile(zip_filename, "w", compression=ZIP_DEFLATED) as zip:
        # writing each file one by one
        for file in files:
            zip.write(file)
    # os.rename(zip_filename, historical_dir + '/' + os.path.basename(zip_filename))

    gateway_session = SSHSession(
        host=TRIPLESTORE_PROXY_GATE,
        username=TRIPLESTORE_SERVER_USER,
        private_key_file=SSH_KEY,
    ).open()
    remote_session = gateway_session.get_remote_session(
        host=triplestore_ip, private_key_file=SSH_KEY
    )
    scp = SCPClient(remote_session.ssh_transport)
    scp.put(zip_filename, remote_path=GRAPHDB_DATA_FOLDER)

    print("Files uploaded in {:.2f} seconds".format(time.time() - start_time))

    context = "http://{dataset}-{time}".format(dataset=dataset, time=loading_date)
    import_body = {
        "fileNames": [zip_filename],
        "importSettings": {
            "context": context,
            "baseURI": "http://linked.data.gov.au/dataset/{}/".format(dataset),
        },
    }

    requests.post(
        "https://graphdb.tern.org.au/rest/data/import/server/{}".format(repository),
        headers={"Content-Type": "application/json", "Cache-Control": "no-cache"},
        auth=HTTPBasicAuth(
            user,
            password,
        ),
        json=import_body,
    )

    start_time2 = time.time()
    while True:
        files_response = requests.get(
            "https://graphdb.tern.org.au/rest/data/import/server/{}".format(repository),
            headers={"Accept": "application/json", "Cache-Control": "no-cache"},
            auth=HTTPBasicAuth(
                user,
                password,
            ),
        ).json()
        file_check = list(
            filter(
                lambda file: file.get("name") == zip_filename
                and file.get("status") == "DONE",
                files_response,
            )
        )
        if file_check:
            print("Files imported in {:.2f} seconds".format(time.time() - start_time2))
            break
        print("Importing data...")
        time.sleep(30)

    ssh_cmd = "rm {graphdb_dir}/{file}".format(
        graphdb_dir=GRAPHDB_DATA_FOLDER, file=zip_filename
    )
    remote_session.ssh_client.exec_command(ssh_cmd)

    named_graphs = requests.get(
        "https://graphdb.tern.org.au/repositories/{}/contexts".format(repository),
        headers={"Accept": "application/sparql-results+json"},
        auth=HTTPBasicAuth(
            user,
            password,
        ),
    ).json()["results"]["bindings"]
    print(named_graphs)

    for graph in named_graphs:
        name = graph.get("contextID").get("value")
        if "http://{}".format(dataset) in name and name != context:
            print(
                requests.post(
                    "https://graphdb.tern.org.au/repositories/{}/statements".format(
                        repository
                    ),
                    headers={"Accept": "application/json"},
                    params={"update": "CLEAR GRAPH <{}>".format(name)},
                    auth=HTTPBasicAuth(
                        user,
                        password,
                    ),
                ).status_code
            )

    # map(lambda graph: print(requests.post(
    #     "https://graphdb.tern.org.au/repositories/{}/statements".format(repository),
    #     headers={"Accept": "application/json"},
    #     params={"update": "CLEAR GRAPH <{}>".format(graph.get("contextID").get("value"))},
    #     auth=HTTPBasicAuth(
    #         user,
    #         password,
    #     )).status_code), filter(
    #     lambda graph: dataset in graph.get("contextID").get("value") and graph.get("contextID").get("value") != context,
    #     named_graphs))

    print("Data loaded into GraphDB in {:.2f} seconds".format(time.time() - start_time))


def ingest_anzograph(triplestore_ip, sparql_endpoint, user, password, dataset):
    LOAD_DATA_SPARQL_QUERY = "LOAD with 'global' <dir:/azg-data/**REPLACE_FILENAME**> INTO GRAPH <http://**REPLACE_DATASET**>"
    CLEAR_GRAPH_SPARQL_QUERY = "CLEAR GRAPH <http://**REPLACE_DATASET**>"

    # path = os.path.join(Config.APP_DIR, Config.OUTPUT_DIR)
    path = "/indexer/bdbsa/"
    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if file[-4:] == ".ttl":
                files.append(os.path.join(r, file))

    print("Files:", files)
    print("Number of files:", len(files))

    loading_date = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    historical_dir = path + "/" + loading_date
    if not os.path.exists(historical_dir):
        os.makedirs(historical_dir)

    start_time = time.time()

    for f in files:
        gzip_filename = f + ".gz"
        with open(f, "rb") as f_in:
            with gzip.open(gzip_filename, "wb") as f_out:
                shutil.copyfileobj(f_in, f_out)

        os.rename(gzip_filename, historical_dir + "/" + os.path.basename(gzip_filename))

    tar_filename = "{dataset}_{time}.ttl.gz.tar".format(
        dataset=dataset, time=loading_date
    )
    with tarfile.open(historical_dir + "/" + tar_filename, mode="x:") as archive:
        archive.add(historical_dir, arcname="")
        gateway_session = SSHSession(
            host=TRIPLESTORE_PROXY_GATE,
            username=TRIPLESTORE_SERVER_USER,
            private_key_file=SSH_KEY,
        ).open()
        remote_session = gateway_session.get_remote_session(
            host=triplestore_ip, private_key_file=SSH_KEY
        )
        scp = SCPClient(remote_session.ssh_transport)
    scp.put(historical_dir + "/" + tar_filename, remote_path=ANZOGRAPH_DATA_FOLDER)

    print("Files uploaded in {:.2f} seconds".format(time.time() - start_time))

    start_time = time.time()
    sparql = SPARQLWrapper(sparql_endpoint)
    sparql.setCredentials(user, password)
    sparql.method = "POST"
    sparql.setTimeout(30000)

    sparql.setQuery(CLEAR_GRAPH_SPARQL_QUERY.replace("**REPLACE_DATASET**", dataset))
    sparql.query().convert()

    sparql.setQuery(
        LOAD_DATA_SPARQL_QUERY.replace("**REPLACE_DATASET**", dataset).replace(
            "**REPLACE_FILENAME**", tar_filename
        )
    )
    response = sparql.query().convert()

    ssh_cmd = "mv {anzo_dir}/{file} {anzo_dir}/previous_ingestions/{file}".format(
        anzo_dir=ANZOGRAPH_DATA_FOLDER, file=tar_filename
    )
    remote_session.ssh_client.exec_command(ssh_cmd)

    print(response)
    print(
        "Data loaded into Anzograph in {:.2f} seconds".format(time.time() - start_time)
    )


def silentremove(filename):
    try:
        os.remove(filename)
    except OSError as e:  # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT:  # errno.ENOENT = no such file or directory
            raise  # re-raise exception if a different error occurred


def main(args):
    if args.triplestore == "anzograph":
        ingest_anzograph(
            args.ip, args.sparql_endpoint, args.user, args.password, args.dataset
        )
    elif args.triplestore == "virtuoso":
        ingest_virtuoso(args.ip, args.dataset)
    elif args.triplestore == "graphdb":
        ingest_graphdb(args.ip, args.repository, args.user, args.password, args.dataset)


if __name__ == "__main__":
    args = cli()
    main(args)
